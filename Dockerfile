FROM golang:1.19.1-alpine3.16 as builder

WORKDIR /user

COPY . .

RUN go build -o main cmd/main.go

FROM alpine:3.16

WORKDIR /user

COPY --from=builder /user/main .

CMD [ "/user/main" ]
