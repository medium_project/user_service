CREATE TABLE IF NOT EXISTS "users"(
    "id" SERIAL PRIMARY KEY,
    "first_name" VARCHAR(30) NOT NULL,
    "last_name" VARCHAR(30) NOT NULL,
    "phone_number" VARCHAR(20) UNIQUE,
    "email" VARCHAR(50) NOT NULL UNIQUE,
    "gender" VARCHAR(10) CHECK ("gender" IN('male', 'female')),
    "password" VARCHAR NOT NULL,
    "username" VARCHAR(30) UNIQUE,
    "profile_image_url" VARCHAR,
    "type" VARCHAR(255) CHECK ("type" IN('superadmin', 'user')) NOT NULL,
    "created_at" TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP
);

-- Password; Ahror@4991
INSERT INTO users(first_name, last_name, email, password, type) 
VALUES('axror', 'abdukarimov', 'ahrorahrorovnt@gmail.com', '$2a$10$FswwRaJzdKJpU5faOFkyCev5SCSvmT/uQ8Dg8MvtXAjQgPrFSFmca', 'superadmin');

-- User@123
INSERT INTO users(first_name, last_name, email, password, type) 
VALUES('test', 'testov', 'testuser@gmail.com', '$2a$10$vYIEzLcdiO4pd9tc8kVOP.x2o7ECoC/RRAEO247bMAg5RfHN42a/e', 'user');